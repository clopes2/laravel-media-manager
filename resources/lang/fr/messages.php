<?php

return [
    'create_success' => 'Un nouveau :entity a été créé.',
    'update_success' => 'Le :entity a été mis à jour.',
    'delete_success' => 'Le :entity a été supprimé.',
    'upload_success' => 'Le :entity a été téléchargé.',
    'move_success'   => 'Un nouveau :entity a été déplacé.',
    'rename_success' => 'Un nouveau :entity a été renommé.',

    'create_error' => 'Désolé! une erreur est survenue',
    'delete_error' => 'Désolé! une erreur est survenue',
    'upload_error' => 'Désolé! une erreur est survenue',
    'move_error'   => 'Désolé! une erreur est survenue',
    'rename_error' => 'Désolé! une erreur est survenue',

    'file'=> 'fichier',
    'folder'=> 'dossier',
    'btn' => [
        'add_folder' => 'Nouveau dossier',
        'refresh' => 'Refraichir',
    ],

    'actions'=> [
        'move' => [
            'same' => 'Merci de sélectionner un autre dossier',
            'inception' => 'Le dossier ne peut pas être déplacé dans lui-même',
            'already_exists' => 'Le fichier existe déjà dans ce dossier',
        ],
        'rename' => [
            'already_exists' => 'L\'élément existe déjà dans le dossier'
        ],
        'delete' => [
            'already_exists' => 'Le fichier :entity n\'existe plus',
            'dir_must_be_empty' => 'Le dossier doit être vide pour le supprimer'
        ],
        'new_file' => [
          'bad_mime_type' => 'Ce type de fichier n\'est pas autorisé.',
          'bad_size' => 'Le fichier est trop volumineux',
        ],
        'create_dir' => [
            'already_exists' => 'Le dossier existe déjà',
        ],
        'upload' => [
            'already_exists' => 'Le fichier existe déjà',
        ]
    ]
];
